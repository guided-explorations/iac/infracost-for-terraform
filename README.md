# Infracost with Terraform and GitLab Merge Requests

[Infracosts](https://www.infracost.io/) is an open-source tool that scans your terraform code and checks through cloud price sheets to give simple breakdowns before provisioning resources. 

Infracosts integrates with various CI Tools including GitLab. Infracost scans your terraform plan output and evaluates the resources generated. A docker image is supported to evaluate the plan step in a CI Job, and then generates a merge request comment as a bot to inform decision makers on the costs.

### Getting started


### Merge request screenshot
![logo](images/merge_request.png)

### Reports
![logo](images/cloud_base_price_report.png)
![logo](images/cloud_usage_price_report1.png)
