terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.50.0"
    }
  }

	backend "s3" {
    bucket = "terraform-s3-example"
    key    = "tf-infracost-example/terraform"
		region = "us-east-2"
  }

}

provider "aws" {
  region = "us-east-2" # <<<<< Try changing this to eu-west-1 to compare the costs
}

resource "aws_instance" "web_app" {
  ami           = "ami-00f8e2c955f7ffa9b"
  instance_type = "t2.micro" # <<<<< Try changing this to m5.8xlarge to compare the costs

  root_block_device {
    volume_size = 50
  }

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "io1" # <<<<< Try changing this to gp2 to compare costs
    volume_size = 1000
    iops        = 800
  }
}
